# Grey Software Students

We use this repository to keep track of our students' learning progress. 

# Roadmaps Offered

## Open Source Web Essentials Certificate

- [ ] Complete [our onboarding process](https://onborading.grey.software)
- [ ] Create a [Codepen](https://codepen.io) profile to start logging your web experiments on the platform
- [ ] Complete [Scrimba's Intro HTML / CSS Course](https://scrimba.com/learn/introhtmlcss)
- [ ] Complete upto Part 1 of [Scrimba's Intro JavaScript Course](https://scrimba.com/learn/learnjavascript)
- [ ] Complete [VueMastery's Vue Crash Course](https://www.youtube.com/watch?v=bzlFvd0b65c)
- [ ] Build and deploy a personal website using Vue.js, a utility-based CSS Framework and Markdown.
- [ ] Complete a review process of your personal website during a 1-hour call with a Grey Software mentor
- [ ] Write a short reflection and obtain your certificate
